package com.sda.brigi.advanced.model;

//BirthCertificate - nume, prenume, CNP, sex, loculNasterii, anulNasterii
public class BirthCertificate {

	private String name;
	private String surname;
	private String cnp;
	private String sex;
	private String birthLocation;
	private int birthYear;
	private boolean isBornInRomania;

	// facem un constructor default pt brigi pt ca nu are paramentrii
	public BirthCertificate() {
	}

	// facem un constructor cu parametrii pt Radus Birth Certificate
	public BirthCertificate(String name, String surname, String cnp, String sex, String birthLocation, int birthYear,
			boolean isBornRomania) {
		this.name = name;
		this.surname = surname;
		this.cnp = cnp;
		this.sex = sex;
		this.birthLocation = birthLocation;
		this.birthYear = birthYear;
		this.isBornInRomania = isBornRomania;
	}

	// punem getter si setteri publici pt a putea accesa variabilele private din aceasta clasa si de pachetul celalat de main de clasa StartBirth
	public String getName() {
		return name;

	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurename() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getCnp() {
		return cnp;
	}

	public void setCnp(String cnp) {
		this.cnp = cnp;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {

		this.sex = sex;
	}

	public String getBirthLocation() {
		return birthLocation;
	}

	public void setBirthLocation(String birthLocation) {
		this.birthLocation = birthLocation;
	}

	public int getBirthYear() {
		return birthYear;
	}

	public void setBirthYear(int birthYear) {
		this.birthYear = birthYear;
	}

	public boolean isBornInRomania() {
		return isBornInRomania;
	}

	public void setBornInRomania(boolean isBornInRomania) {
		this.isBornInRomania = isBornInRomania;
	}

	// scris de mana, pt. a ne pune programul automat facem: //click dreapta-Source-GenerateToString
	@Override
	public String toString() {
		return "Name: " + this.name + " Surname: " + this.surname + " CNP: " + this.cnp + " sex:  " + this.sex
				+ " BithLOcation " + this.birthLocation + " BirthYear:  " + this.birthYear + " isBornInRomania:  "
				+ this.isBornInRomania();
	}

}
