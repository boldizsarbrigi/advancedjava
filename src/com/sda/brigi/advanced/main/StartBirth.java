package com.sda.brigi.advanced.main;

import com.sda.brigi.advanced.model.BirthCertificate;

public class StartBirth {
	// BirthCertificate - nume, prenume, CNP, sex, loculNasterii, anulNasterii
	public static void main(String[] args) {

		BirthCertificate brigiBirthCertificate = new BirthCertificate();
		BirthCertificate radusBirthCertificate = new BirthCertificate("Radu", "Trest", "2880711314020", "M", "Cluj",
				2010, false);

		System.out.println("Brigis Birth Certificate:");
		brigiBirthCertificate.setName("Brigi");
		brigiBirthCertificate.setSurname("Boldizsar");
		brigiBirthCertificate.setCnp("2880711314010");
		brigiBirthCertificate.setSex("F");
		brigiBirthCertificate.setBirthLocation("Zalau");
		brigiBirthCertificate.setBirthYear(1988);
		brigiBirthCertificate.setBornInRomania(true);
		System.out.println(brigiBirthCertificate.getName());
		System.out.println(brigiBirthCertificate.getSurename());
		System.out.println(brigiBirthCertificate.getCnp());
		System.out.println(brigiBirthCertificate.getSex());
		System.out.println(brigiBirthCertificate.getBirthLocation());
		System.out.println(brigiBirthCertificate.getBirthYear());
		System.out.println(brigiBirthCertificate.isBornInRomania());

		System.out.println();
		System.out.println("Radus Birth Certificate:");
		System.out.println(radusBirthCertificate.getName());
		System.out.println(radusBirthCertificate.getSurename());
		System.out.println(radusBirthCertificate.getCnp());
		System.out.println(radusBirthCertificate.getSex());
		System.out.println(radusBirthCertificate.getBirthLocation());
		System.out.println(radusBirthCertificate.getBirthYear());
		System.out.println(radusBirthCertificate.isBornInRomania());

		// apelam toString din clasa BirthCertificate
		System.out.println("calling toString: "+radusBirthCertificate.toString());
		
		//acesta este folosit de zi cu zi fara explicit cu toString
		System.out.println("no call: "+ radusBirthCertificate);
	}

}
